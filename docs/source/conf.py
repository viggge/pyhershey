# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../../src'))

import pyhershey


# -- Project information -----------------------------------------------------

project = 'pyhershey'
copyright = '2021, viggge'
author = 'viggge'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'autoapi.extension',
    'sphinx.ext.napoleon',
    'sphinx.ext.viewcode',
    'recommonmark',
    'sphinxcontrib.images',
    'sphinx.ext.autosectionlabel'
]

autoapi_type = 'python'
autoapi_dirs = ['../../src']

autoapi_keep_files = True

autoapi_add_toctree_entry = False

autoapi_options = [
    "members",
    "undoc-members",  # this is temporary until we add docstrings across the codebase
    "show-inheritance",
    "show-module-summary",
    "special-members",
    # "imported-members",
    "inherited-members",
]

autosectionlabel_prefix_document = True

# autoapi_template_dir = '_autoapi_templates'


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "pydata_sphinx_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_logo = "_static/logo.png"


def setup(app):

    import pathlib

    import matplotlib.pyplot as plt

    from docutils import nodes
    from docutils.statemachine import ViewList
    from docutils.parsers.rst import Directive
    from sphinx.util.nodes import nested_parse_with_titles
    from docutils.parsers.rst import directives

    from pyhershey import glyph_factory
    from pyhershey.show import plot_glyph

    class GlyphsDirective(Directive):
        has_content = False
        required_arguments = 1
        optional_arguments = 0
        option_spec = {}
        final_argument_whitespace = False

        def run(self):
            collection = directives.uri(self.arguments[0])
            if collection == 'all':
                glyph_iter = glyph_factory.index_iterator()
            else:
                glyph_iter = glyph_factory.index_iterator(collection)
            thumbnails = ViewList()

            for glyph_index in glyph_iter:
                thumbnails.append('.. thumbnail:: /glyphs/{}.png\n'.format(glyph_index), 'foo', 1)
                thumbnails.append('    :title: Glyph {}\n'.format(glyph_index), 'foo', 1)
                thumbnails.append('    :width: 24.5%\n', 'foo', 1)
                thumbnails.append('    :group: glyphs\n\n', 'foo', 1)

            node = nodes.section()
            node.document = self.state.document

            nested_parse_with_titles(self.state, thumbnails, node)

            # And return the result.
            return node.children

    app.add_directive('glyphs', GlyphsDirective)

    import multiprocessing as mp

    def save_glyph_as_image(glyph, out_path):
        def impl():
            fig, ax = plt.subplots()
            plot_glyph(glyph, ax)

            ax.set_aspect('equal')
            fig.tight_layout()
            fig.savefig(out_path, bbox_inches='tight', dpi=300)

        # prevent memory buildup because matplotlib is not cleaning up properly
        p = mp.Process(target=impl)
        p.start()
        p.join()
        

    glyphs_path = pathlib.Path(app.srcdir + '/glyphs')

    try:
        os.mkdir(glyphs_path)
    except FileExistsError:
        pass

    for glyph_index in glyph_factory.index_iterator():
        # print('Processing glyph', glyph_index)
        glyph_path = glyphs_path.joinpath(f'{glyph_index}.png')
        if not glyph_path.is_file():
            save_glyph_as_image(glyph_factory.from_index(glyph_index), glyph_path)

    categories_path = pathlib.Path(app.srcdir + '/categories')
    try:
        os.mkdir(categories_path)
    except FileExistsError:
        pass

    for mapping in glyph_factory.collections:
        category_path = categories_path.joinpath(f'{mapping}.rst')
        with open(category_path, 'w') as fp:
            heading = '{}\n'.format(mapping)
            fp.write(heading)
            fp.write(('=' * (len(heading) - 1) + '\n'))
            fp.write('.. glyphs:: {}\n\n'.format(mapping))

    # with open(pathlib.Path(app.srcdir).joinpath('glyphsbycategorie.rst'), 'w') as fp:
    #     heading = 'Glyphs by category\n'
    #     fp.write(heading)
    #     fp.write(('=' * (len(heading) - 1) + '\n'))

    #     fp.write('.. toctree::\n')
    #     for mapping in glyph_factory.collections:
    #         fp.write('    categories/{}\n'.format(mapping))

