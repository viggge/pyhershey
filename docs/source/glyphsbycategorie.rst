Glyphs by category
==================

A list of all available glyphs. To get the glyph index, click on its rendered image.

**Available ASCII mappings:** cyrillic_complex, gothic_german_triplex, greek_complex_small, greek_plain, greek_simplex, italic_complex, italic_complex_small, italic_triplex, roman_complex, roman_complex_small, roman_duplex, roman_plain, roman_simplex, roman_triplex, script_complex, script_simplex


.. toctree::
    :hidden:

    categories/oriental
    categories/occidental
    categories/mathematical
    categories/daggers
    categories/astronomical
    categories/astrological
    categories/musical
    categories/miscellaneous
    categories/cyrillic_complex
    categories/gothic_german_triplex
    categories/gothic_italian_triplex
    categories/greek_complex_small
    categories/greek_plain
    categories/greek_simplex
    categories/italic_complex
    categories/italic_complex_small
    categories/italic_triplex
    categories/roman_complex
    categories/roman_complex_small
    categories/roman_duplex
    categories/roman_plain
    categories/roman_simplex
    categories/roman_triplex
    categories/script_complex
    categories/script_simplex
