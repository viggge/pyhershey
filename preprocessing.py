import argparse
import tempfile
import os
import re
import subprocess as sp
from typing import List, Mapping, Any, Tuple, Pattern, Match, Optional, Iterator, Dict
from functools import reduce
from operator import iconcat
import lzma

import numpy as np

import toml


def to_sh_files(tempdir: str, font_data_dir: str) -> None:
    for filename in [f'part{i}' for i in range(1, 6)]:
        with open(f'{font_data_dir}/{filename}', 'r') as fp:
            file = fp.readlines()
        first_line_of_sh = next((i for i, x in enumerate(file) if x == '#!/bin/sh\n'), None)
        if not first_line_of_sh:
            raise RuntimeError

        # We have to fix some lines in the files
        # Lines of type
        #   if test 26458 -ne "`wc -c 'hersh.oc1'`"
        # are changed to
        #   if test 26458 -ne `wc -c < 'hersh.oc1'`
        fix_test_pattern = re.compile(r'if test +\d')
        for i, line in enumerate(file):
            if fix_test_pattern.findall(line):
                new_line = line.replace('"', '')
                index = new_line.find('wc -c')
                new_line = new_line[:index + 5] + ' <' + new_line[index + 5:]
                file[i] = new_line

        with open(f'{tempdir}/{filename}.sh', 'w') as fp:
            fp.write(''.join(file[first_line_of_sh:]))


def concatenate_raw_glyph_files(tempdir: str, raw_glyph_file: str) -> None:
    line_begin_pattern = re.compile(r'^(?=.{5})[ ]+[\d]+')

    file_data = []
    for file in [raw_glyph_file + str(i) for i in range(1, 5)]:
        with open(f'{tempdir}/{file}', 'r') as fp:
            lines = fp.readlines()

        fixed_lines = []
        for line in lines:
            if not line_begin_pattern.match(line):
                if line.strip():
                    fixed_lines[-1] = fixed_lines[-1][:-1] + line
            else:
                fixed_lines.append(line)

        file_data.extend(fixed_lines)

    with open(f'{tempdir}/{raw_glyph_file}', 'w') as fp:
        fp.writelines(file_data)


def parse_glyph_line(line: str) -> Mapping[str, Any]:
    def to_abs(char: str):
        return ord(char) - ord('R')

    def coord_to_tuple(coord):
        return to_abs(coord[0]), to_abs(coord[1])

    def tokenize(line: str):
        # glyph index, number of vertices, left pos, right pos
        tokens: List[str] = [line[0:5], line[5:8], line[8], line[9]]

        # vertices (len(line)-1 and not len(line) to skip '\n' at the end
        for i in range(10, len(line)-1, 2):
            tokens.append(line[i:i+2])

        return tokens

    glyph: Mapping[str, Any] = {}

    tokens: List[str] = tokenize(line)

    glyph['index'] = int(tokens[0])
    n_vertices_with_pen_up: int = int(tokens[1])
    glyph['left_pos'] = to_abs(tokens[2])
    glyph['right_pos'] = to_abs(tokens[3])

    pen_up_token = ' R'
    state_pen_down = 'PEN_DOWN'
    state_pen_up = 'PEN_UP'

    state = state_pen_up
    segments: List[List[Tuple[int, int]]] = []

    for token in tokens[4:]:
        if state == state_pen_up:
            if token == pen_up_token:
                raise RuntimeError
            segments.append([coord_to_tuple(token)])

            state = state_pen_down
        else:  # state == state_pen_down
            if token == pen_up_token:
                state = state_pen_up
            else:
                segments[-1].append(coord_to_tuple(token))

    # do some checks on glyph

    n_parsed_vertices = len(reduce(iconcat, segments, []))
    n_parsed_segments = len(segments)
    if n_parsed_vertices + n_parsed_segments != n_vertices_with_pen_up:
        # handle special case: glyphs with no vertices (spacers I guess) have n_vertices_with_pen_up == 1, but no
        # the we don't have any real segments. In this case, don't throw an exception.
        if not segments and n_vertices_with_pen_up != 1:
            raise RuntimeError

    glyph['segments'] = [np.array(segment) for segment in segments]
    glyph['n_vertices'] = n_parsed_vertices
    glyph['n_segments'] = n_parsed_segments

    return glyph


def parse_glyphs(tempdir: str, conc_glyph_file: str) -> List[Mapping[str, Any]]:
    with open(f'{tempdir}/{conc_glyph_file}', 'r') as fp:
        glyph_lines = fp.readlines()

    glyphs: List[Mapping[str, Any]] = []

    for glyph_line in glyph_lines:
        try:
            glyphs.append(parse_glyph_line(glyph_line))
        except Exception as e:
            raise RuntimeError(f'Error in parsing line:\n"{glyph_line}"') from e

    return glyphs


def parse_hmp_file(tempdir: str, file: str) -> List[int]:
    glyph_indices: List[int] = []

    range_pattern: Pattern[str] = re.compile(r'(\d{1,4})-(\d{1,4})')
    list_pattern: Pattern[str] = re.compile(r'(^\d{1,4}(?![-\d])|(?<!^)(?<![-\d])\d{1,4})')

    with open(f'{tempdir}/{file}', 'r') as fp:
        hmp: List[str] = fp.readlines()

    for line in hmp:
        range_match: Optional[Match[str]] = range_pattern.match(line)
        if range_match:
            beg, end = range_match.group(1, 2)
            # print(beg, end)
            beg = int(beg)
            end = int(end)
            # end+1 ??
            glyph_indices.extend(list(range(beg, end+1)))

        else:
            list_matches: Iterator[Match[str]] = list_pattern.finditer(line)
            glyph_indices.extend(
                map(lambda match: int(match.group(1)), list_matches)
            )

    return glyph_indices


def parse_ascii_mappings(tempdir: str) -> Mapping[str, Any]:
    files = {
        'cyrilc': {'font_style': 'Cyrillic', 'font_type': 'complex'},
        'gothgbt': {'font_style': 'Gothic German', 'font_type': 'triplex'},
        'gothgrt': {'font_style': 'Gothic German', 'font_type': 'triplex'},
        'gothitt': {'font_style': 'Gothic Italian', 'font_type': 'triplex'},
        'greekcs': {'font_style': 'Greek', 'font_type': 'complex small'},
        'greekp': {'font_style': 'Greek', 'font_type': 'plain'},
        'greeks': {'font_style': 'Greek', 'font_type': 'simplex'},
        'italicc': {'font_style': 'Italic', 'font_type': 'complex'},
        'italiccs': {'font_style': 'Italic', 'font_type': 'complex small'},
        'italict': {'font_style': 'Italic', 'font_type': 'triplex'},
        'romanc': {'font_style': 'Roman', 'font_type': 'complex'},
        'romancs': {'font_style': 'Roman', 'font_type': 'complex small'},
        'romand': {'font_style': 'Roman', 'font_type': 'duplex'},
        'romanp': {'font_style': 'Roman', 'font_type': 'plain'},
        'romans': {'font_style': 'Roman', 'font_type': 'simplex'},
        'romant': {'font_style': 'Roman', 'font_type': 'triplex'},
        'scriptc': {'font_style': 'Script', 'font_type': 'complex'},
        'scripts': {'font_style': 'Script', 'font_type': 'simplex'},
    }

    ascii_mappings = {}

    for file, info in files.items():
        mapping = parse_hmp_file(tempdir, file + '.hmp')
        name = f"{info['font_style']}_{info['font_type']}".lower().replace(' ', '_')
        ascii_mappings[name] = mapping

    return ascii_mappings


def map_glyph_list_to_dict(glyph_list: List[Mapping[str, Any]]):
    glyph_dict: Mapping[str, Any] = {}
    for glyph in glyph_list:
        index = str(glyph['index'])
        del glyph['index']
        glyph_dict[str(index)] = glyph

    return glyph_dict


def glyph_metric(left_pos: float, right_pos: float, segments: List[np.ndarray]) -> Dict[str, Any]:

    if segments:
        flattened = np.vstack(segments)
    else:
        flattened = np.array([[0, 0]])

    min_x, min_y = np.min(flattened, axis=0)
    max_x, max_y = np.max(flattened, axis=0)

    width = abs(min_x - max_x)
    heigth = abs(min_x - max_x)

    left_bearing = min_x - left_pos
    right_bearing = max_x - right_pos

    # assert min_x <= left_pos
    # assert max_x >= right_pos

    return {
        'bounding_box': ((min_x, min_y), ((max_x, max_y))),
        'width': width, 'height': heigth,
        'advance_width': abs(left_pos) + abs(right_pos),
        'left_bearing': abs(left_bearing), 'right_bearing': abs(right_bearing),
        'descent': min_y, 'ascent': max_x, 
    }


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--temp', default=None, type=str, help='Path to the temporary dictionary to be used.')
    args = parser.parse_args()

    font_data_dir = 'font-data/'

    tempdir = None
    if not args.temp:
        tempdir_obj = tempfile.TemporaryDirectory()
        tempdir = tempdir_obj.name
    else:
        tempdir = args.temp
        try:
            os.mkdir(tempdir)
        except FileExistsError:
            print('Warning: Probably overwriting files.')

    # First step: convert part1 - part5 to proper *.sh files
    to_sh_files(tempdir, font_data_dir)

    # Second step: run *.sh files
    for filename in [f'part{i}.sh' for i in range(1, 6)]:
        sp.check_call(['sh', filename], cwd=tempdir)

    # Third step: concatenate hersh.or* and hersh.oc* to hersh.or and hersh.oc, respectively.
    #             Additionally, fix some line break problems (sometimes, a glyph description is wrapped over more
    #             than one line. This is fixed here).
    concatenate_raw_glyph_files(tempdir, 'hersh.or')
    concatenate_raw_glyph_files(tempdir, 'hersh.oc')

    # Fourth step: parse glyph files to glyph dicts and change the indices of the oriental glyphs to be above
    # the occidental one (this is done by adding 4000 to the glyph indices)
    oc_glyphs = parse_glyphs(tempdir, 'hersh.oc')

    def map_index(glyph):
        glyph['index'] += 4000
        return glyph

    or_glyphs = [map_index(glyph) for glyph in parse_glyphs(tempdir, 'hersh.or')]

    # Fifth step: parse mapping files for occidental font style
    #

    ascii_mappings = parse_ascii_mappings(tempdir)

    # Sixth step: assign glyphs to various collections

    glyph_type_collection = {
        'oriental': [glyph['index'] for glyph in or_glyphs],
        'occidental': [glyph['index'] for glyph in oc_glyphs]
    }

    def glyph_range(begin, end):
        return list(range(begin, end + 1))

    symbol_collection = {
        'mathematical': (
               glyph_range(227, 229) + [232] + glyph_range(724, 729) + [732] + glyph_range(737, 740)
               + glyph_range(1227, 1254) + glyph_range(1256, 1270) + glyph_range(2227, 2270) + glyph_range(1401, 1412)
               + glyph_range(2294, 2295) + glyph_range(2401, 2412)
        ),
        'daggers': glyph_range(1276, 1279) + glyph_range(2276, 2279),
        'astronomical': glyph_range(1281, 1239) + glyph_range(2281, 2293),
        'astrological': glyph_range(2301, 2312),
        'musical': glyph_range(2317, 2332) + glyph_range(2367, 2382),
        # 'typestting': [],
        'miscellaneous': []
    }

    categorized_glyph_indices = []

    for glyphs in ascii_mappings.values():
        categorized_glyph_indices.extend(glyphs)
    for glyphs in symbol_collection.values():
        categorized_glyph_indices.extend(glyphs)

    categorized_glyph_indices = set(categorized_glyph_indices)
    all_glyph_indices = glyph_type_collection['occidental'] + glyph_type_collection['oriental']

    miscellaneous_glyphs = [
        glyph_index for glyph_index in all_glyph_indices if glyph_index not in categorized_glyph_indices and glyph_index < 4000
    ]

    symbol_collection['miscellaneous'] = miscellaneous_glyphs


    # some sanity checking
    for mapping, indices in {**symbol_collection, **ascii_mappings}.items():
        for index in indices:
            if index not in all_glyph_indices:
                raise RuntimeError(f'glyph {index} of {mapping} is not in all_glyph_indices')

    glyphs = oc_glyphs + or_glyphs

    # Seventh step: Flip the glyphs on the x axis and shift them so the baseline is at y=0. Very small glyphes are
    # shifted by 5 up, small by 6 and ALL other by 9

    very_small_glyph_indices = []
    small_glyph_indices = []
    for mapping, indices in ascii_mappings.items():
        if 'plain' in mapping:
            very_small_glyph_indices.extend(indices)
        elif 'complex_small' in mapping:
            small_glyph_indices.extend(indices)

    for glyph in glyphs:
        shift_y = 9
        if glyph['index'] in very_small_glyph_indices:
            shift_y = 4
        elif glyph['index'] in small_glyph_indices:
            shift_y = 6

        for segment in glyph['segments']:
            segment[:, 0] -= glyph['left_pos']
            segment[:, 1] *= -1
            segment[:, 1] += shift_y

        glyph.update(glyph_metric(0, glyph['right_pos'] - glyph['left_pos'], glyph['segments']))

        del glyph['left_pos']
        del glyph['right_pos']

    # Eight step: build database and copy to source directory
    database = {
        'glyphs': map_glyph_list_to_dict(glyphs),
        'mappings': {
            **ascii_mappings
        },
        'collections': {
            **glyph_type_collection,
            **symbol_collection,
            **ascii_mappings
        }
    }

    # print(ascii_mappings.keys())

    toml_database = toml.dumps(database, encoder=toml.TomlNumpyEncoder())

    with open('src/pyhershey/database.toml.xz', 'wb') as fp:
        compressed_database = lzma.compress(toml_database.encode('utf-8'))
        fp.write(compressed_database)

    with open(f'{tempdir}/database.toml', 'w') as fp:
       fp.write(toml_database)

    # Finally: Cleanup
    try:
        tempdir_obj.cleanup()
    except UnboundLocalError:
        # a custom directory was defined
        pass


if __name__ == '__main__':
    main()
