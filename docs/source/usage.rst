Usage
=====

Getting started
---------------

Import the module with ::

    from pyhershey import glyph_factory

The database containing all glyphs is loaded automatically and can be interfaced via the ``glyph_factory`` object.

Glyphs can be created either by using the glyph index or an ASCII mapping. The latter maps ASCII characters to glyphs defined in the hershey font.

To create a glyph by index, use ::

    glyph = glyph_factory.from_index(50)


Similar, to get a glyph with specified ASCII mapping, use ::

    glyph = glyph_factory.from_ascii('x', 'roman_complex')

where the second parameter is the name of the mapping.

You can find all available mappings and glyphs together with their indices in the :ref:`glyphs <glyphsbycategorie:Glyphs by category>` section.

The glyph object stores several metrics about the glyph as well as the line segments the glyph is composed of as properties. E.g. ::

    print('segments:', glyph.segments)
    print('width:', glyph.width)


See :class:`~pyhershey.glyph_view.GlyphView` for a complete list of properties.


Displaying glyphs
-----------------

If the package is installed with the matplotlib dependency, glyphs can be displayed by ::

    from pyhershey import glyph_factory
    from pyhershey.show import show_glyph

    show_glyph(glyph_factory.from_index(50))


This plots the glyph using matplotlib. If you like to modify the plot, you can use :func:`~pyhershey.show.plot_glyph` which plots the glyph to an user provided axes object.

Simple text shaping
-------------------

The library comes with a very primitive text shaping utility. This shape_text function takes an ASCII text as input and outputs a list of glyphs an their positions.
New lines can be created with ``\n``. See the function's documentation for a full list of available parameters :func:`here <pyhershey.shape_text.shape_text>`.

Example:: 

    from pyhershey import shape_text

    shaped_glyphs = shape_text('This is\ntest', advance_width=16, )

    print('shaped glyphs: ')

    for shaped_glyph in shaped_glyphs:
        print('glyph_index:', shaped_glyph['glyph'].index, ', pos:', shaped_glyph['pos'])


The shaped glyphs can be displayed directly with the :func:`~pyhershey.show.show_shaped_glyphs` function. ::

    from pyhershey.show import show_shaped_glyphs


    show_shaped_glyphs(shaped_glyphs)
